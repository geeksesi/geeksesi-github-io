<?php
/**
*
*/
class Controller
{

	public $url;
	private $parsdown;
	private $config;
	private $model;
	private $view;

	function __construct()
	{
		include 'libs.php';
		$this->parsdown = new Parsedown();
		$model = new Model();
		$view = new View();
		$this->model = $model;
		$this->view = $view; 
		$this->url();

		$this->controller();
	}


	public function controller()
	{
		// var_dump($this->url);
		if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on") 
		{ 
		    $port = "http://";
		}
		else
		{
		  $port = "https://";
		}
		$controller = [];
		$controller["base_url"] = $port.$_SERVER["SERVER_NAME"]."/"; 
		if ($this->url === null)
		{
			$controller["location"] = "home";
			$this->view->page_home();
			$controller["url"] = $port.$_SERVER["SERVER_NAME"]."/";
		}
		elseif ($this->url === false)
		{
			$controller["location"] = "404";
			$this->view->error_404();
			$controller["url"] = $port.$_SERVER["SERVER_NAME"]."/";
		}
		elseif (isset($this->url["lang"]))
		{
			$language = $this->url["lang"];
			$this->model->set_lang($language);
			
			// ## set theme texts ##
			$text_array = $this->model->get_array("text");
			$controller["text"]["footer"] = $text_array["footer"];
			$controller["text"]["public"] = $text_array["public"];
			$controller["text"]["header"] = $text_array["header"];
			$controller["lang"] = $language;

			// ** ** //
			$controller["url"] = $port.$_SERVER["SERVER_NAME"]."/".$language."/";
			// ## END set theme texts ##

			if (isset($this->url["cp"])) 
			{
				if($this->url["cp"] == "post")
				{
					if (isset($this->url["data"])) 
					{
						// echo "Hello";
						$controller["single"] = $this->post();
						if ($controller["single"] === false) 
						{
							$controller["location"] = "404";
							$controller["url"] = $port.$_SERVER["SERVER_NAME"]."/";
							$this->view->error_404();
						}
						else
						{
							$controller["location"] = "single";
							$controller["single"]["content"] = $this->parsdown->text($controller["single"]["content"]);
							$controller["single"]["title"] = str_replace("-", " ", $this->url["data"]);
							$this->view->single($controller);
						}
					}
					else
					{
						$controller["location"] = "main";
						$controller["page_main"] = $this->page_main(6);
						$this->view->page_main($controller);
					}
					
				}
				elseif($this->url["cp"] == "category")
				{
					if (isset($this->url["data"])) 
					{
						
					}
					else
					{
						echo "Hello";
					}
				}
				if($this->url["cp"] == "page")
				{
					if (isset($this->url["data"])) 
					{
						
					}
				}
				if($this->url["cp"] == "tag")
				{
					if (isset($this->url["data"])) 
					{
						
					}
				}
			}
			else
			{
				// ## means we are in the language main page 
				// To Do
				// * set controller location 
				// * get data of $this->page_main(); and save it in :> $controller["main_page"]
				// * send $controller to view :: $this->view->page_main();
				error_log("i'm Else");
				$controller["location"] = "main";
				$controller["page_main"] = $this->page_main(6);
				$this->view->page_main($controller);
			}
		}
		// var_dump($controller);
	}

	public function page_main($_limit)
	{
		if (!isset($_limit)) 
		{
			return false;
		}
		$post_array = $this->model->get_array("posts");
		$limit_key = $post_array[0] - $_limit +1;
		if ($limit_key < 1) 
		{
			$limit_key = 1;
		}
		$key = array_search($limit_key, array_column($post_array, 'id'));
		$column_array = array_column($post_array, 'id');
		$column_array[] = "num";
		$new_array = array_combine($column_array, $post_array);
		$key++;
		$post_array[0]++;
		for ($i=$key; $i < $post_array[0] ; $i++) 
		{ 
		
			$page_main[$i]["title"] = $new_array[$i]["title"];
			$page_main[$i]["url"] = str_replace(" ", "-", $new_array[$i]["title"]);
			$page_main[$i]["date"] = $new_array[$i]["date"];
		}
		return $page_main;
	}


	public function post()
	{
		if (!isset($this->url["data"])) 
		{
			return false;
		}
		$post_name = $this->url["data"];
		$data = $this->model->single_post($post_name);
		return $data;
	}

	public function tag()
	{

	}

	public function cat()
	{

	}

	public function page()
	{

	}


	private function url()
	{
		if (!isset($_GET["url"]) || $_GET["url"] === '')
		{
			if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] !== '/') 
			{
				// var_dump($_SERVER['REQUEST_URI']);
				$main_url = substr($_SERVER['REQUEST_URI'], 1);
			}
			else
			{
				return null;
			}
		}
		else 
		{
			$main_url = $_GET["url"];
		}
		$url         = [];
		$explode     = explode("/", $main_url);
		$check_array =
		[
			"lang" =>
			[
				"fa",
				"az",
				"en"
			],
			"cp" =>
			[
				"category",
				"post",
				"page",
				"tag"
			]
		];
		foreach ($check_array["lang"] as $lkey => $lvalue)
		{
			if ($lvalue == $explode[0])
			{
				$url["lang"] = $lvalue;
			}
		}
		if (!isset($url["lang"]) && isset($explode[0])) 
		{
			$this->url = false;
			return false;
		}
		elseif (!isset($url["lang"]))
		{
			return false;
		}
		if (isset($explode[1]))
		{
			if (empty($explode[1]) && !isset($explode[2])) 
			{
				$this->url = $url;
				return $url;
			}
			elseif (empty($explode[1]) && isset($explode[2])) 
			{
				$this->url = false;
				return false;
			}
			foreach ($check_array["cp"] as $ckey => $cvalue)
			{
				if ($cvalue == $explode[1])
				{
					$url["cp"] = $cvalue;
				}
			}
			
			if (!isset($url["cp"]))
			{
				$this->url = false;
				return false;
			}
			elseif (isset($explode[2]))
			{
				if (empty($explode[2])) 
				{
					
				}
				else
				{
					$url["data"] = $explode[2];
				}
			}
		}
		$this->url = $url;
		return true;
	}
}
