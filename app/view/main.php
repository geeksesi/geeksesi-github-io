<?php include __DIR__."/header.php"; ?>
  <div class="contai="contai="contai="contai="container-fluid">
      <br><br><br><br>
      <div class="container">
        <div class="row" style="width: 100%;padding: 0;">
          <h1 class="text-success" style="text-align: center;width: 100%;padding: 0;margin-bottom: 15px" alt="بسم الله الرحمن الرحیم">﷽</h1>
        </div>
        <div class="row">
          <div class="col-lg-8 posts">
            <?php foreach ($_data["page_main"] as $key => $value): 
              ?>
            <ul >
              <li><a href="<?php echo $_data["url"]; ?>post/<?php echo $value['url']; ?>"><h3><i class="fab fa-angellist"></i>&nbsp;<?php echo $value["title"]; ?></h3></a></li>
              <li><h4><i class="fas fa-calendar-alt"></i>&nbsp;&nbsp;<time><?php echo str_replace("-", " / ", $value["date"]); ?></time></h4></li>
            </ul>
            <?php endforeach; ?>    
          </div>
          <?php include __DIR__."/sidebar.php"; ?>
        </div>
      </div>
    </div>
    <br><br><br><br><br>
    <?php include __DIR__."/footer.php"; ?>

<?php

