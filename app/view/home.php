<!DOCTYPE html>
<html>
<head>
	<title>Geeksesi::Javad | وبلاگ محمد جواد قاسمی</title>
	<link rel="stylesheet" type="text/css" href="assets/style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link href="https://use.fontawesome.com/releases/v5.0.2/css/all.css" rel="stylesheet">
</head>
<body id="Home">
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<img src="assets/img/geeksesi_W.png">
			</div>
			<div class="col-md-2"></div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-4 az">
				<a href="https://geeksesi.ir/az" class="btn btn-secondary disabled" role="button" aria-disabled="true">Azərbaycan</a>
			</div>
			<div class="col-md-4 fa ">
				<a href="/fa" class="btn hvr-float-shadow"><img  src="assets/img/IR_FLAG.png" alt=""></a>
			</div>
			<div class="col-md-4 en">
				<a href="https://geeksesi.ir/en" class="btn btn-secondary disabled" role="button" aria-disabled="true">English</a>
			</div>
		</div>
	</div>
</body>
</html>