<footer class="mainfooter" role="contentinfo">
  <div class="footer-top p-y-2">
    <div class="container-fluid">
    </div>
  </div>
  <div class="footer-middle">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <!--Column1-->
        <div class="footer-pad">
         <h4><i class="fas fa-external-link-alt"></i>&nbsp;<?php echo $_data["text"]["footer"]["New Portfolio"][0]; ?></h4>
          <ul class="list-unstyled">
            <?php
            foreach ($_data["text"]["footer"]["New Portfolio"] as $keyp => $valuep) 
            {
              if ($keyp === 0) 
              {
                break;
              }
              echo "<li><a href='{$valuep}'>{$keyp}</a></li>";
            }
            ?>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <!--Column1-->
        <div class="footer-pad">
          <h4><i class="fas fa-smile"></i>&nbsp;<?php echo $_data["text"]["footer"]["Our Friends"][0]; ?></h4>
          <ul class="list-unstyled">
            <?php
            foreach ($_data["text"]["footer"]["Our Friends"] as $keyp => $valuep) 
            {
              if ($keyp === 0) 
              {
                break;
              }
              echo "<li><a href='{$valuep}'>{$keyp}</a></li>";
            }
            ?>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <!--Column1-->
        <div class="footer-pad">
          <h4><i class="fab fa-stack-overflow"></i>&nbsp;<?php echo $_data["text"]["footer"]["Best Lesson"][0]; ?></h4>
          <ul class="list-unstyled">
            <?php
            foreach ($_data["text"]["footer"]["Best Lesson"] as $keyp => $valuep) 
            {
              if ($keyp === 0) 
              {
                break;
              }
              echo "<li><a href='{$valuep}'>{$keyp}</a></li>";
            }
            ?>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <!--Column1-->
        <div class="footer-pad">
          <h4><i class="fas fa-link"></i>&nbsp;<?php echo $_data["text"]["footer"]["Other Links"][0]; ?></h4>
          <ul class="list-unstyled">
            <?php
            foreach ($_data["text"]["footer"]["Other Links"] as $keyp => $valuep) 
            {
              if ($keyp === 0) 
              {
                break;
              }
              echo "<li><a href='{$valuep}'>{$keyp}</a></li>";
            }
            ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <!--Footer Bottom-->
          <p class="text-xs-center">&copy;&nbsp;GNU GPL v3</p>
        </div>
      </div>
    </div>
  </div>
</footer>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
</body>
</html>