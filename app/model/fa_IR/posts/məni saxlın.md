## معرفی
پی‌اچ‌پی (به انگلیسی: PHP) یک زبان برنامه‌نویسی است که برای طراحی وب توسعه یافته‌است، اما می‌توان از آن به عنوان یک زبان عمومی نیز استفاده‌کرد. تا ژانویهٔ سال ۲۰۱۳ میلادی پی‌اچ‌پی بر روی ۲۴۴ میلیون وب‌گاه و ۲٫۱ میلیون سرور وب نصب شده‌است. این زبان در سال ۱۹۹۵ میلادی توسط راسموس لِردورف (به انگلیسی: Rasmus Lerdorf) ساخته‌شد و در حال حاضر توسعهٔ آن بر عهدهٔ گروه پی‌اچ‌پی می‌باشد. در ابتدا پی‌اچ‌پی از عبارت صفحهٔ خانگی شخصی (به انگلیسی: Personal Home Page) گرفته شده‌بود. اما اکنون این کلمه مخففِ بازگشتی PHP: Hypertext Preprocessor به معنی پی‌اچ‌پی: پیش‌پردازندهٔ ابرمتن می‌باشد.([ویکی‌پدیا](https://fa.wikipedia.org/wiki/%D9%BE%DB%8C%E2%80%8C%D8%A7%DA%86%E2%80%8C%D9%BE%DB%8C))

## پیش‌نیاز‌ها
- یک توزیع گنو/لینوکسی 
- پکیج‌منیجر `apt` یا `yum` یا `dnf` 
- دسترسی به روت سیستم عامل و یا یوزر `sudo`
- [کارساز وب آپاچی ](https://blog.domain-host.ir/%d9%86%d8%ad%d9%88%d9%87-%d9%86%d8%b5%d8%a8-%d9%88%d8%a8%e2%80%8c-%d8%b3%d8%b1%d9%88%d8%b1-%d8%a2%d9%be%d8%a7%da%86%db%8c-%d8%af%d8%b1-%d8%a7%d9%88%d8%a8%d9%88%d9%86%d8%aa%d9%88-16%d8%8c4/)

## نصب پی‌اچ‌پی
 نصب اخرین نسخه پایدار `php` یعنی نسخه `7.0` 

### اوبونتو : 
```
# apt update	
# apt install php7.0 php7.0-cgi libapache2-mod-php
```
### فدورا :
```
# dnf install php php-common
```
### سنت‌او‌اس
```
# yum install php php-common
```
----
تا اینجا شما `php` رو در اختیار دارید اما شما نیاز به نصب پکیج های دیگری هم دارید که باید به انتخاب و بسته به کار شما نصب شوند لیست یک سری از پکیج ها را در زیر می‌توانید مشاهده‌کنید

```
	* OPcache (php-opcache) – The Zend OPcache provides faster PHP execution through opcode caching and optimization.
    * APCu (php-apcu) – APCu userland caching
    * CLI (php-cli) – Command-line interface for PHP
    * PEAR (php-pear) – PHP Extension and Application Repository framework
    * PDO (php-pdo) – A database access abstraction module for PHP applications
    * MySQL (php-mysqlnd) – A module for PHP applications that use MySQL databases
    * PostgreSQL (php-pgsql) – A PostgreSQL database module for PHP
    * MongoDB (php-mongodb) – PHP MongoDB database driver
    * Redis (php-redis) – Extension for communicating with the Redis key-value store
    * Memcache (php-memcache) – Extension to work with the Memcached caching daemon
    * Memcached (php-memcached) – Extension to work with the Memcached caching daemon
    * GD (php-gd) – A module for PHP applications for using the gd graphics library
    * XML (php-xml) – A module for PHP applications which use XML
    * MBString (php-mbstring) – A module for PHP applications which need multi-byte string handling
    * MCrypt (php-mcrypt) – Standard PHP module provides mcrypt library support
```

از همین حالا شما می تونید از `php` در وب‌سرور خودتون استفاده کنید اما برای اولویت دادن به اجرای فایل `index.php` در کارساز آپاچی مسیر زیر رو طی کرده و فایل `dir.conf`رو اجرا و دستور زیر رو  جایگزین این کد کنید :
```
<IfModule mod_dir.c>
    DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm
</IfModule>
```
کد زیر رو جایگزین کنید.
```
<IfModule mod_dir.c>
    DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
</IfModule>
```
خب حالا برای امتحان کردن و دیدن نتیجه کار درون پوشه آپاچی در مسیر `var/www/html` یک فایل با اسم `info.php` ایجاد کرده و محتوای زیر رو درون اون قرار بدید .
```
<?php
phpinfo();
```
اگر با نتیجه زیر مواجه شدید کار موفقیت‌آمیز بوده در غیر این صورت یا در مراحل قبل مشکلی پیش اومده یا شما نیاز به فعال کردن `mode` هایی دارید که نصبشون کردید با دستور زیر می تونید مود های نصب شده رو فعال کنید :
```
sudo a2enmod MODE_NAME
```
