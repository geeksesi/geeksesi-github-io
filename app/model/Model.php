<?php

/**
*
*/
class Model
{
	private $post;

	/**
	 * [__constract description]
	 * @param  [type] $_lang [description]
	 * @return [type]        [description]
	 */
	public function set_lang($_lang)
	{
		if (!isset($_lang)) {
			return false;
		}
		// include  __DIR__.$_lang."/index.php";
		switch ($_lang)
		{
			case 'fa':

				include  __DIR__."/fa_IR/fa_IR.php";
				$this->post = new fa_IR();
				return true;
				break;

			case 'en':

				include  __DIR__."/en_US/en_US.php";
				$this->post = new en_US();
				return true;
				break;

			case 'az':

				include  __DIR__."/az_AZ/az_AZ.php";
				$this->post = new az_AZ();
				return true;
				break;

			default:
				return false;
				break;
		}
	}

	/**
	 * [single_post description]
	 * @param  [type] $_url  [description]
	 * @return [type]        [description]
	 */
	public function single_post($_url)
	{
		global $web_url;
		if (!isset($_url) || !isset($this->post))
		{
			return false;
		}

		$single["content"] = $this->post->post_content("post", $_url);
		if ($single["content"] === false)
		{
			var_dump($_url);
			return false;
		}
		if (file_exists(IMAGE_DIR.$_url.".jpg"))
		{
			$single["cover"] = $web_url."assets/img/".$_url.".jpg";
		}
		else
		{
			return false;	
		}

		return $single;

	}

	/**
	 * @param  [type]
	 * @return [type]
	 */
	public function get_array($_name)
	{
		if (!isset($_name) || !isset($this->post))
		{
			return false;
		}
		switch ($_name) {
			case 'posts':
				return $this->post->data("post");
				break;
			case 'category':
				return $this->post->data("category");
				break;
			case 'text':
				return $this->post->data("text");
				break;
			case 'tag':
				return $this->post->data("tag");
				break;
			case 'pages':
				return $this->post->data("page");
				break;

			default:
				return false;
				break;
		}
	}

	/**
	 * [get_category description]
	 * @param  [type] $_category [description]
	 * @param  [type] $_id       [description]
	 * @return [type]            [description]
	 */
	public function get_category($_category = null , $_id = null)
	{
		if (!isset($this->post))
		{
			return false;
		}
		$category_return = null;
		if ($_category !== null && $_id !== null )
		{
			return false;
		}
		elseif ($_category == null && $_id == null)
		{
			return false;
		}
		elseif(isset($_category))
		{
			if (isset($this->category[$_category]))
			{
				$category_return["url"]   = $_category;
				$category_return["value"] = $this->category[$_category];
			}
			return category_return;
		}
		elseif (isset($_id))
		{
			foreach ($this->category as $ckey => $cvalue)
			{
				if ($cvalue["id"] == $_id)
				{
					$category_return["url"] = $ckey;
					$category_return["value"] = $cvalue;
				}
				return $category_return;
			}
		}
		return false;
	}

	/**
	 * [category_posts description]
	 * @param  [type] $_id [description]
	 * @return [type]      [description]
	 */
	public function category_posts($_id)
	{
		if (!isset($id))
		{
			return false;
		}
		$all_posts = $this->get_array("posts");
		if (!is_array($all_posts))
		{
			return false;
		}
		$i = 0;
		$category_post = null;
		foreach ($all_posts as $post_key => $post_value)
		{
			if (isset($post_value["category"]) && is_array($post_value["category"]))
			{
				foreach ($$post_value["category"] as $cat_key => $cat_value)
				{
					if ($cat_value == $_id )
					{
						$category_post[$i]["url"]         = $post_key;
						$category_post[$i]["id"]          = $post_value["id"];
						$category_post[$i]["title"]       = $post_value["title"];
						$category_post[$i]["description"] = $post_value["description"];
						$category_post[$i]["date"]        = $post_value["date"];
						$category_post[$i]["image"]       = $post_value["image"];
						$i++;
					}
				}
			}
		}
		return $category_post;
	}
	public function tag_posts($_id)
	{
		if (!isset($id))
		{
			return false;
		}
		$all_posts = $this->get_array("tags");
		if (!is_array($all_posts))
		{
			return false;
		}
		$i = 0;
		$tag_post = null;
		foreach ($all_posts as $post_key => $post_value)
		{
			if (isset($post_value["tag"]) && is_array($post_value["tag"]))
			{
				foreach ($$post_value["tag"] as $cat_key => $cat_value)
				{
					if ($cat_value == $_id )
					{
						$tag_post[$i]["url"]         = $post_key;
						$tag_post[$i]["id"]          = $post_value["id"];
						$tag_post[$i]["title"]       = $post_value["title"];
						$tag_post[$i]["description"] = $post_value["description"];
						$tag_post[$i]["date"]        = $post_value["date"];
						$tag_post[$i]["image"]       = $post_value["image"];
						$i++;
					}
				}
			}
		}
		return $tag_post;
	}
}
